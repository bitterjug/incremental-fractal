module Main exposing (main)

import Browser
import Browser.Events exposing (onAnimationFrameDelta)
import Canvas exposing (Renderable)
import Canvas.Settings
import Color
import Html exposing (Html)
import Html.Attributes exposing (style)


type alias Point =
    ( Float, Float )


type alias Line =
    { from : Point
    , to : Point
    }


type Msg
    = Frame Float


type alias Stackframe =
    { point : Point
    , theta : Float
    , depth : Int
    }


type alias Model =
    { line : Maybe Line
    , stack : List Stackframe
    }


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscription
        }


subscription : Model -> Sub Msg
subscription model =
    if model.stack == [] then
        Sub.none
    else
        onAnimationFrameDelta Frame


width : number
width =
    1000


height : number
height =
    800


iterations : Int
iterations =
    10


firstPoint : Point
firstPoint =
    ( width / 2, height - 100 )


initialAngle : number
initialAngle =
    -90


init : () -> ( Model, Cmd Msg )
init _ =
    ( { line =
            Just
                { from = firstPoint
                , to = nextPoint firstPoint initialAngle iterations
                }
      , stack =
            [ { point = firstPoint
              , theta = initialAngle
              , depth = iterations
              }
            ]
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Frame _ ->
            ( newModel model, Cmd.none )


nextPoint : Point -> Float -> Int -> Point
nextPoint ( x, y ) theta depth =
    ( x + (cos (degrees theta) * toFloat depth * 8)
    , y + (sin (degrees theta) * toFloat depth * 8)
    )


newModel : Model -> Model
newModel ({ stack } as model) =
    case stack of
        [] ->
            model

        { point, theta, depth } :: tail ->
            if depth == 1 then
                { line = Nothing
                , stack = tail
                }
            else
                let
                    newEndPoint =
                        nextPoint point theta depth
                in
                { line =
                    Just
                        { from = point
                        , to = newEndPoint
                        }
                , stack =
                    [ { point = newEndPoint
                      , depth = depth - 1
                      , theta = theta - 29
                      }
                    , { point = newEndPoint
                      , depth = depth - 1
                      , theta = theta + 23
                      }
                    ]
                        ++ tail
                }


view : Model -> Html Msg
view model =
    model.line
        |> Maybe.map (paint >> List.singleton)
        |> Maybe.withDefault []
        |> Canvas.toHtml ( width, height )
            [ style "bdepth" "10px solid rgba(0,0,0,0.1)" ]


paint : Line -> Renderable
paint { from, to } =
    Canvas.shapes [ Canvas.Settings.stroke Color.darkYellow ]
        [ Canvas.path from [ Canvas.lineTo to ] ]
