# Elm meetup - side-effects

## The problem

We got this Java code to draw a fractal tree; we want to convert it to Elm.

```java
public void paintComponent(Graphics g) {
  g.setColor(Color.RED);
  draw(g, depth, topX, topY, 90, 200.00);
}

public void draw(Graphics g, int depth, int x1, int y1, double angle, double length){
    int x2, y2, x3, y3; 
    double newAngle = Math.toRadians(angle); 

    if (depth == 1) {  
        return;    
    } else {
        x2 = (x1 - (int)Math.round(Math.cos(newAngle) * depth * 10));
        y2 = (y1 - (int)Math.round(Math.sin(newAngle) * depth * 10));
        
        g.drawLine(x1, y1, x2, y2);
        
        draw(g, depth-1, x2, y2, angle+30, length+20);
        draw(g, depth-1, x2, y2, angle-30, length+20);
    }
} 
```

It works by recursion; the call stack mirrors the structure of the tree

![1569269334182](1569269334182.png)

## It’s recursive, that’s already functional isn’t it?

Well kinda. But `drawLine` works by a side effect.

Elm has no way to describe the execution of a side-effect. Normally we ask the runtime to do them, by returning a` Cmd Msg` from `update`. 

## But in Elm we use the view to draw graphics

![1569270411286](1569270411286.png)

The Elm Way is to use recursion to build a model of the entire view, then render it, [like this](https://ellie-app.com/6Kth5yf7Zjqa1).

That delay for the calculation can easily become too long. Could we do it incrementally?

## Canvas is weird

Canvas is weird because we use a declarative-looking statements to render to canvas from the `model`. And we do this in the `view`:

![1569275541182](weird.png)

But those statements **do** have side-effects!

The result of one view sticks around in the canvas for the next call; if you want to erase something from the canvas you must overwrite it with the background colour!

## So we can render one line at a time with Canvas in Elm

We need the Elm runtime to call `view` for each canvas update. So this is **not** recursive!

We are going to:

- Each `update` stores _no more than one_ line in the `Model` for `view` to render.

  ```elm
  type alias Model =
  	{ Maybe Line
  	, ... 
  	}
  ```

- Update gets called on animation frame, and exits each time. No function calls itself recursively!

- Each time `update` calculates the next line to draw (if any), and stores the remaining work to do in the model

- Model the call stack explicitly in Elm (use a list, and pattern matching) 

  ```elm
  type alias Model =
  	{ ...
  	, stack: List Stackframe
  	}
  ```

    ```elm
    newModel : Model -> Model
    newModel ({ stack } as model) =
        case stack of
            [] ->
                -- nothing to do
                model
    
            stackFrame :: tail ->
    			...
    ```

- Stack Frame contains the arguments to the missing recursive call.

  ```elm
  type alias Stackframe =
      { depth : Int
      , point : Point
      , theta : Float
      }
  ```

  _Recursive_ calls become stack frames pushed on the stack (after popping the current call arguments).
  
  ```elm
  	{ model | 
  		stack = 
  			{depth = depth - 1, point = newPoint ... , theta = theta - 20 } 
  				:: tail
  	}
  ```
  
- To get update called we subscribe to the animation frame (if there’s any more work to do) with [onAnimationFrame](https://package.elm-lang.org/packages/elm/browser/latest/Browser-Events#onAnimationFrame) 

  ```elm
  type Msg = Frame Float
  ```

	```elm
  subscription : Model -> Sub Msg
  subscription model =
      if model.stack == [] then
          Sub.none
      else
          onAnimationFrameDelta Frame
	```

	```elm
  update : Msg -> Model -> ( Model, Cmd Msg )
  update msg model =
      case msg of
          Frame _ ->
              ( newModel model, Cmd.none )
	```

- Termination condition: pop a stack frame, no line to draw

  ```elm
              if depth == 0 then
                  { line = Nothing
                  , stack = tail
                  }
  ```
  


After we did this work I watched [The Best Refactoring You've Never Heard Of](http://www.pathsensitive.com/2019/07/the-best-refactoring-youve-never-heard.html) and it turns out this is an example of _Defunctionalizing continuations_.

